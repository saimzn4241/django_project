from django.shortcuts import render
from datetime import datetime, timedelta
from .models import *
from django.contrib.auth.models import User
from django.http import JsonResponse

def index(request):
    user = User.objects.get(username = "superuser" )
    profile = Profile.objects.get(user = user)
    category = Category.objects.all()
    subtitle = Subtitle.objects.all()

    timelines = []
    for each in category:
        temp = {}
        temp['category'] = each
        temp['timeline'] = []
        t=Timeline.objects.filter(category = each)
        for item in t:
            temp['timeline'].append(item)

        timelines.append(temp)
    services = Services.objects.all()
    return render(request, "index.html", {
        "user": user,
        "profile": profile,
        "timelines": timelines,
        "services": services,
        "subtitle": subtitle,
    })

def message(request):
    data = {}

    if request.method == "POST":
        name = request.POST.get("name")
        email = request.POST.get("email")
        message = request.POST.get("message")
        print("name=", name)
        msg = Message(
            name = name,
            email = email,
            message = message,
        )
        msg.save()

        data['message'] = "Your message was successfully sent."
        data['type'] = "success"

        return JsonResponse(data)
