# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-23 12:36
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0004_profile_country'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='profilePhoto',
        ),
    ]
