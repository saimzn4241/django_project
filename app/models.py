from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from ckeditor_uploader.fields import RichTextUploadingField
from django_countries.fields import CountryField

CategoryType = (
    (1, "Education"),
    (2, "Experience"),
    (3, "Internships"),
    (4, "Trainings"),
)

class Subtitle(models.Model):
    date_added = models.DateTimeField(default=timezone.now, null=True, blank=True)
    title = models.CharField(max_length=150, null=True, blank=True)
    def __str__(self):
        return self.title

class Profile(models.Model):
    date_added = models.DateTimeField(default=timezone.now, null=True, blank=True)
    user = models.OneToOneField(User, null=True, blank=True)
    name = models.CharField(max_length=150, null=True, blank=True)
    age = models.IntegerField(null=True, blank=True)
    address = models.CharField(max_length=150, null=True, blank=True)
    country = CountryField(null=True, blank=True)
    email = models.EmailField(max_length=70, null=True, blank=True)
    phone = models.IntegerField(null=True, blank=True)
    freelance = models.CharField(max_length=150, null=True, blank=True)
    lon = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)
    lat = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)
    resume =  models.FileField(upload_to="resume/", blank=True, null=True, max_length=200)
    profilePhoto = models.FileField(upload_to="profile/", blank=True, null=True, max_length=200)
    content = RichTextUploadingField(blank=True, null=True)
    githubLink = models.CharField(max_length=150, null=True, blank=True)
    twitterLink =  models.CharField(max_length=150, null=True, blank=True)
    facebookLink =  models.CharField(max_length=150, null=True, blank=True)
    instagramLink =  models.CharField(max_length=150, null=True, blank=True)

    def __str__(self):
        return self.name

class Category(models.Model):
    date_added = models.DateTimeField(default=timezone.now, null=True, blank=True)
    title = models.CharField(max_length=150, null=True, blank=True)
    def __str__(self):
        return self.title

class Timeline(models.Model):
    date_added = models.DateTimeField(default=timezone.now, null=True, blank=True)
    category = models.ForeignKey(Category, null=True, blank=True)
    title = models.CharField(max_length=150, null=True, blank=True)
    period = models.CharField(max_length=150, null=True, blank=True)
    place = models.CharField(max_length=150, null=True, blank=True)
    description = models.CharField(max_length=500, null=True, blank=True)
    def __str__(self):
        return self.title

class Services(models.Model):
    date_added = models.DateTimeField(default=timezone.now, null=True, blank=True)
    title = models.CharField(max_length=150, null=True, blank=True)
    description = models.CharField(max_length=500, null=True, blank=True)
    photo = models.FileField(upload_to="services/",null=True, blank=True)
    def __str__(self):
        return self.title

class Message(models.Model):
    date_added = models.DateTimeField(default=timezone.now, null=True, blank=True)
    name = models.CharField(max_length=150, null=True, blank=True)
    email = models.EmailField(max_length=70, null=True, blank=True)
    message = models.CharField(max_length=1000, null=True, blank=True)
    def __str__(self):
        return self.name
