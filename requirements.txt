Django==1.11.3
django-ckeditor==5.3.0
django-js-asset==0.1.1
netifaces==0.10.6
olefile==0.44
Pillow==4.2.1
pytz==2017.2
django-countries
